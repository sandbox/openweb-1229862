<?php


interface legal_holidays_country {

  public function getCountry();
  public function getStates(); 
  public function getHolidays();

}