<?php

define('LEGAL_HOLIDAYS_COUNTRY_NAME', 'Germany');


class legal_holidays_germany implements legal_holidays_country {
  
  public function getCountry() {
    return t(LEGAL_HOLIDAYS_COUNTRY_NAME);
  }

  public function getStates() {
    return array(
      'bw' => t('Baden-Württemberg'),
      'by' => t('Bayern'),
      'be' => t('Berlin'),
      'bb' => t('Brandenburg'),
      'hb' => t('Bremen'),
      'hh' => t('Hamburg'),
      'he' => t('Hessen'),
      'mv' => t('Mecklenburg-Vorpommern'),
      'ni' => t('Niedersachen'),
      'nw' => t('Nordrhein-Westfalen'),
      'rp' => t('Rheinland-Pfalz'),
      'sl' => t('Saarland'),
      'sn' => t('Sachsen'),
      'st' => t('Sachsen-Anhalt'),
      'sh' => t('Schleswig-Holstein'),
      'th' => t('Thüringen'),
    );
  } 

  public function getHolidays($strYear = NULL) {
    if (!$strYear) $strYear = date('Y');
    $arrStates = $this->getStates();
    $easterdate = easter_date(intval($strYear));
    $days = 60 * 60 * 24;
    return array(
      '0101' => array (
        'name' => t('Neujahr'),
        'state' => array_keys($arrStates),
      ),
      '0601' => array(
        'name' => t('Heilige Drei Könige'),
        'state' => array('bw', 'by', 'st'),
      ),
      '0105' => array(
        'name' => t('Tag der Arbeit'),
        'state' => array_keys($arrStates),
      ),
      '1508' => array(
        'name' => t('Mariä Himmelfahrt'),
        'state' => array('by', 'sn'),
      ),
      '0310' => array(
        'name' => t('Tag der Deutschen Einheit'),
        'state' =>  array_keys($arrStates),
      ),
      '0310' => array(
        // TODO: in "sn" this is called "Reformationsfest"
        'name' => t('Reformationstag'),
        'state' =>  array ('bw', 'bb', 'mv', 'sn', 'st', 'th'),
      ),
      '0310' => array(
        'name' => t('Allerheiligen'),
        'state' =>  array ('bw', 'by', 'nw', 'rp', 'sl'),
      ),
      '2512' => array(
        'name' => t('Erster Weihnachtstag'),
        'state' =>  array_keys($arrStates),
      ),
      '2612' => array(
        'name' => t('Zweiter Weihnachtstag'),
        'state' =>  array_keys($arrStates),
      ),
      date('dm', $easterdate - 3 * $days) => array(
        'name' => t('Gründonnerstag'),
        'state' => array('by'),
      ),
      date('dm', $easterdate - 2 * $days) => array(
        'name' => t('Karfreitag'),
        'state' => array_keys($arrStates),
      ),
      date('dm', $easterdate + 1 * $days) => array(
        'name' => t('Ostermontag'),
        'state' => array_keys($arrStates),
      ),
      date('dm', $easterdate + 39 * $days) => array(
        'name' => t('Christi Himmelfahrt'),
        'state' => array_keys($arrStates),
      ),
      date('dm', $easterdate + 50 * $days) => array(
        'name' => t('Pfingstmontag'),
        'state' => array_keys($arrStates),
      ),
      date('dm', $easterdate + 50 * $days) => array(
        'name' => t('Fronleichnam'),
        'state' => array ('bw', 'by', 'he', 'nw', 'rp', 'sl', 'sn', 'th'),
      ),
      date('dm', strtotime("-11 days", strtotime("1 sunday", mktime(0,0,0,11,26,$strYear)))) => array(
        'name' => t('Buß- und Bettag'),
        'state' => array ('sn'),
      ),
    );
}